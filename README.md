# React kata junior

This is an exercise for assessing one's frontend programming skills.
The guidelines for this kata can be found at : https://github.com/planity/test_recrutement/tree/master/frontend_junior


## Installation

Install the project using npm : 
```bash
npm i
```

## Launching the app

Launch the app with npm : 
```bash
npm start
```
