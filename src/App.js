import EmailForm from 'components/EmailForm/EmailForm';
import React from 'react';
import './App.css';

function App() {
  return (
      <>
        <h1 className="app-title">React Kata Email Input</h1>
        <EmailForm />
      </>
  )
}

export default App;