import providers from 'assets/providers.json';
import EmailValidator from 'email-validator';

// given the part of an email adress, returns the 3 best matches for the email provider
export function filteredProviders(email) {
    if (EmailValidator.validate(email)) {
        return [];
    }
    if (email != null && email.includes('@')) {
        // keep only the first 3 providers beginning with the string following '@'
        const suggestions = providers
            .filter(p => p.startsWith(email.split('@')[1]))
            .slice(0, 3);
        if (suggestions.length !== 0) {
            return suggestions
        }
    }
    return providers.slice(0, 3);
}

// replaces the provider of an email adress (String) with a new provider 
export function replaceProvider(email, newProvider) {
    if (email != null && email.length >0) {
        if (email.includes('@')) {
            return email.split('@')[0] + '@' + newProvider;
        }
        return email + '@' + newProvider;
    }
}
