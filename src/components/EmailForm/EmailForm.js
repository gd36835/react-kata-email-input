import React, { useState } from 'react';
import './EmailForm.css';
import { filteredProviders, replaceProvider } from '../../utils/utils.js';

const EmailForm = () => {
  let emailInput = null;
  const [email, setEmail] = useState('');

  // edits the email field to replace its provider with the one in input of this function
  function selectProvider(provider) {
    setEmail(replaceProvider(email, provider));
    emailInput.focus();
  }

  return (
    <div className="wrapper">
      <form>
        <fieldset>
          <label htmlFor="email">
            <p>Email</p>
          </label>
          <div >
            <input
              id="email"
              name="email"
              onChange={event => setEmail(event.target.value) } 
              value={email || ''}
              ref={(i) => { emailInput = i; }} />
            {filteredProviders(email).map((provider, i) => (
              <button
                type="button"
                className="provider-button"
                key={i}
                onClick={() => selectProvider(provider)} >
                @{provider}
              </button>
            ))}
          </div>
        </fieldset>
      </form>
    </div>
  )
};

export default EmailForm;