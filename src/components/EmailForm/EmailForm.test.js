import { render, screen } from '@testing-library/react';
import EmailForm from './EmailForm';
import userEvent from '@testing-library/user-event';

beforeEach(() => {
    render(<EmailForm />);
});

test('input should be present', async () => {
    const input = screen.getByRole('textbox', { name: "Email" });

    expect(input).toBeInTheDocument();
});

test('component should display first 3 providers', () => {
    const buttons = screen.getAllByRole("button");

    expect(buttons.length).toEqual(3);
    expect(buttons[0]).toContainHTML('@orange.fr');
    expect(buttons[1]).toContainHTML('@outlook.fr');
    expect(buttons[2]).toContainHTML('@gmail.com');
});

test('clicking button first should do nothing', async () => {
    const input = screen.getByRole('textbox', { name: "Email" });
    await userEvent.click(screen.getAllByRole('button')[0]);

    expect(input).toHaveValue('');
});

test('clicking button when no input should do nothing', async () => {
    const input = screen.getByRole('textbox');
    await userEvent.type(input, 'test');
    await userEvent.clear(input);
    
    await userEvent.click(screen.getAllByRole('button')[0]);

    expect(screen.getByRole("textbox")).toHaveValue('');
});

test('clicking button when no "@" should append provider', async () => {
    const input = screen.getByRole('textbox', { name: "Email" });
    await userEvent.type(input, 'test');
    await userEvent.click(screen.getAllByRole('button')[0]);

    expect(input).toHaveValue('test@orange.fr');
});

test('clicking button when "@" present should replace provider', async () => {
    const input = screen.getByRole('textbox', { name: "Email" });
    await userEvent.type(input, 'test@or');
    await userEvent.click(screen.getAllByRole('button')[0]);

    expect(input).toHaveValue('test@orange.fr');
});

test('typing valid email address should remove suggestions', async () => {
    const input = screen.getByRole('textbox', { name: "Email" });
    await userEvent.type(input, 'test@gmail.com');

    expect(screen.queryByRole("button")).toBeNull();
});

test('typing part of a provider should display correct suggestion', async () => {
    const input = screen.getByRole('textbox', { name: "Email" });
    await userEvent.type(input, 'test@gma');

    const buttons = screen.getAllByRole("button");
    expect(buttons.length).toEqual(1);
    expect(buttons[0]).toContainHTML('@gmail.com');
});

test('typing part of provider with no suggestion should display first 3 providers', async () => {
    const input = screen.getByRole('textbox', { name: "Email" });
    await userEvent.type(input, 'test@aaaaaaaaa');

    const buttons = screen.getAllByRole("button");
    expect(buttons.length).toEqual(3);
    expect(buttons[0]).toContainHTML('@orange.fr');
    expect(buttons[1]).toContainHTML('@outlook.fr');
    expect(buttons[2]).toContainHTML('@gmail.com');
});

test('clicking button should keep focus', async () => {
    const input = screen.getByRole('textbox', { name: "Email" });
    await userEvent.type(input, 'test');
    await userEvent.click(screen.getAllByRole('button')[0]);

    expect(document.activeElement).toEqual(input);
});